module NEXT_RECIEVING_SEQ(
    input clk, resetn, valid_data,
    output reg [11:0] NEXT_RECIEVING_SEQ
);

    always @(posedge clk)begin
        if (!resetn) begin
            NEXT_RECIEVING_SEQ <= 0;
        end else begin
            if (valid_data) begin
                NEXT_RECIEVING_SEQ <= NEXT_RECIEVING_SEQ + 1;
            end
        end
    end

endmodule