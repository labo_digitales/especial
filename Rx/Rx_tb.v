`include "Rx.v"

module Rx_tb();

reg reset, clk, DLLP_TLP_signal, TLP_start, DLLP_start;
reg [7:0] Tx_in;
wire valid_CRC, Sch_DLLPs;
wire [7:0] DLLPs, TLP_out; 
wire [11:0] ACK_NACK_time;

initial begin

    $dumpfile("dumpfile_RX.vcd");
    $dumpvars();
    /*Empiezan pruebas CRC checker dentro del Rx*/
    reset <= 0;
    DLLP_TLP_signal <= 0; 
    TLP_start <= 0;
    DLLP_start <= 0;
    Tx_in <= 0;
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 1; //Empieza DLLP
    Tx_in <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 1; //Empieza DLLP
    Tx_in <= 8'b11001100; //Manda el DLLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 0; //Empieza CRC
    Tx_in <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 0; //Empieza CRC
    Tx_in <= 8'b00110011; //Manda el CRC
    @(posedge clk);
    DLLP_TLP_signal <= 0; //Manda DLLP
    reset <= 1;
    TLP_start <= 0; 
    DLLP_start <= 1; //Empieza DLLP
    Tx_in <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 1; //Empieza DLLP
    Tx_in <= 8'b11110000; //Manda el DLLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 0; //Empieza CRC
    Tx_in <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 0; //Empieza CRC
    Tx_in <= 8'b01010101; //Manda el CRC
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 1; //Empieza DLLP
    Tx_in <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 1; //Empieza DLLP
    Tx_in <= 8'b11001100; //Manda el DLLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 0; //Empieza CRC
    Tx_in <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 0; //Manda DLLP
    TLP_start <= 0; 
    DLLP_start <= 0; //Empieza CRC
    Tx_in <= 8'b00110011; //Manda el CRC*/
    @(posedge clk);
    DLLP_start <= 1; 
    @(posedge clk);
    DLLP_start <= 1;
    @(posedge clk);
    /*Terminan pruebas CRC checker dentro del Rx*/
    /*Empiezan pruebas LCRC checker dentro del Rx*/
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 1; 
    TLP_start <= 1;
    DLLP_start <= 0;
    Tx_in <= 8'b10010100; //Manda el TLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 1; 
    TLP_start <= 0;
    DLLP_start <= 0;
    Tx_in <= 8'b00010101; //Manda el LCRC
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 1; 
    TLP_start <= 1;
    DLLP_start <= 0;
    Tx_in <= 8'b11110000; //Manda el TLP
    @(posedge clk);
    reset <= 1;
    DLLP_TLP_signal <= 1; 
    TLP_start <= 0;
    DLLP_start <= 0;
    Tx_in <= 8'b01110001; //Manda el LCRC
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    $finish;

end
initial clk <= 0;
always #1 clk <= ~clk;

Rx rx(
    /*AUTOINST*/
      // Outputs
      .valid_CRC			(valid_CRC),
      .Sch_DLLPs			(Sch_DLLPs),
      .DLLPs				(DLLPs),
      .TLP_out				(TLP_out),
      .ACK_NACK_time			(ACK_NACK_time),
      // Inputs
      .clk				(clk),
      .resetn				(reset),
      .DLLP_TLP_signal			(DLLP_TLP_signal),
      .TLP_start			(TLP_start),
      .DLLP_start			(DLLP_start),
      .Tx_in				(Tx_in));

endmodule
