module RX_BUFFER(
    input clk, resetn, valid_data,
    input [7:0] TLPs,
    input [11:0] next_rec_seq,
    output reg [7:0] TLP_TranLayer,
    output reg Sch_DLLPs
);

    wire valid_seq;
    reg valid_flop, first_datum;
    reg [7:0] TLP_flop0, TLP_flop1, TLP_flop2;

    always @(posedge clk) begin
        if (!resetn) begin
            TLP_TranLayer <= 0;
            Sch_DLLPs <= 0;
            TLP_flop0 <= 0;
            TLP_flop1 <= 0;
            TLP_flop2 <= 0;
            valid_flop <= 0;
            first_datum <= 0;
        end else begin
            TLP_flop0 <= TLPs;
            TLP_flop1 <= TLP_flop0;
            TLP_flop2 <= TLP_flop1;
            valid_flop <= valid_data;
            if (valid_data) begin
                first_datum <= 1;
            end
            if (first_datum) begin
                if (valid_flop) begin
                    if (valid_seq) begin
                        TLP_TranLayer <= TLP_flop2;
                        Sch_DLLPs <= 0;
                    end else begin
                        Sch_DLLPs <= 1;   
                    end 
                end
                else begin 
                    Sch_DLLPs <= 1;
                end
            end
        end
    end

    SEQ_CHECK seq_check(
        .clk(clk),
        .resetn(resetn),
        .valid_data(valid_data),
        .next_rec_seq(next_rec_seq),
        .valid_seq(valid_seq)
    );

endmodule

module SEQ_CHECK(
    input clk, resetn, valid_data,
    input [11:0] next_rec_seq,
    output reg valid_seq
);

    reg [11:0] seq;

    always @(posedge clk)begin
        if (!resetn) begin
            valid_seq <= 0;
            seq <= 0;
        end else begin
            if (valid_data) begin
                if (seq == next_rec_seq) begin
                    valid_seq <= 1;
                    seq <= seq + 1;
                end else begin
                    valid_seq <= 0;
                end 
            end else begin //Revisar bien esta parte
                valid_seq <= 0;
            end
        end
    end

endmodule