`include "DLLP_DEMUX.v"
`include "LCRC_CALC_CHECK.v"
`include "NEXT_RECIEVING_SEQ.v"
`include "RX_BUFFER.v"
`include "ACK_NACK_LATENCY_TIMER.v"
`include "CRC_CALC_CHECK.v"

module tb_test();

reg reset, clk, TLP_DLLP, start_TLP, start_DLLP;
reg [7:0] DLLP_Demux, TLPs_LCRC, DLLPs_CRC;
wire [7:0] DLLPs, TLPs, TLP_TranLayer; 
wire valid_LCRC, Sch_DLLPs, valid_CRC;
wire [11:0] NEXT_RECIEVING_SEQ, ACK_NACK_time;

initial begin

    $dumpfile("dumpfile.vcd");
    $dumpvars();
    reset <= 0;
    TLP_DLLP <= 0;
    DLLP_Demux <= $random;
    TLPs_LCRC <= $random;
    start_TLP <= 0;
    start_DLLP <= 0;    
    DLLPs_CRC <= 8'h11;
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b10010100; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 0;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b00010101; //Manda el LCRC
    start_TLP <= 0;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'b11001100; //Manda el DLLP
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b10101010; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 0;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b01010101; //Manda el LCRC
    start_TLP <= 0;
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'b00110011; //Manda el CRC
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b11110000; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 0;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b01110001; //Manda el LCRC
    start_TLP <= 0;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'b01100010; //Manda el DLLP
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= $random; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'h0; //Manda los 0's del CRC
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 0;
    DLLP_Demux <= $random;
    TLPs_LCRC <= $random; //Manda el LCRC
    start_TLP <= 0;
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'hFF; //Manda el CRC
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'b11001100; //Manda el DLLP
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'b00110011; //Manda el CRC
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b10010100; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 0;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b00010101; //Manda el LCRC
    start_TLP <= 0;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'b11001100; //Manda el DLLP
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b10101010; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 0;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b01010101; //Manda el LCRC
    start_TLP <= 0;
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'b00110011; //Manda el CRC
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b10010100; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    reset <= 1;
    TLP_DLLP <= 1;
    DLLP_Demux <= $random;
    TLPs_LCRC <= 8'b10010100; //Manda el TLP
    start_TLP <= 1;
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'hFF; //Manda los 0's del DLLP
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'b11001100; //Manda el DLLP
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'b00110011; //Manda el CRC
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'b11001100; //Manda el DLLP
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'b00110011; //Manda el CRC
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'h00; //Manda los 0's del DLLP
    @(posedge clk);
    start_DLLP <= 1;    //Empieza el DLLP
    DLLPs_CRC <= 8'b00110011; //Manda el DLLP
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'h00; //Manda los 0's del CRC
    @(posedge clk);
    start_DLLP <= 0;    //Empieza el CRC
    DLLPs_CRC <= 8'b11001100; //Manda el CRC*/
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    
    $finish;

end
initial clk <= 0;
always #1 clk <= ~clk;

DLLP_DEMUX Rx_demux(
    .clk(clk), 
    .resetn(reset),
    .TLP_DLLP(TLP_DLLP),
    .DLLP_Demux(DLLP_Demux),
    .DLLPs(DLLPs), 
    .TLPs(TLPs)
);

LCRC_CALC_CHECK LCRC_check(
    .clk(clk), 
    .resetn(reset), 
    .start_TLP(start_TLP), 
    .TLPs(TLPs_LCRC),
    .valid_LCRC(valid_LCRC)
);

NEXT_RECIEVING_SEQ next_seq(
    .clk(clk), 
    .resetn(reset),
    .valid_data(valid_LCRC),
    .NEXT_RECIEVING_SEQ(NEXT_RECIEVING_SEQ)
);

RX_BUFFER rx_buffer(
    .clk(clk), 
    .resetn(reset),
    .valid_data(valid_LCRC),
    .TLPs(TLPs_LCRC),
    .next_rec_seq(NEXT_RECIEVING_SEQ),
    .TLP_TranLayer(TLP_TranLayer),
    .Sch_DLLPs(Sch_DLLPs)
);

ACK_NACK_LATENCY_TIMER nack_timer(
    .clk(clk),
    .resetn(reset), 
    .Sch_DLLPs(Sch_DLLPs),
    .ACK_NACK_time(ACK_NACK_time)
);

CRC_CALC_CHECK CRC_check(
    .clk(clk),
    .resetn(reset), 
    .star_DLLP(start_DLLP),
    .DLLPs(DLLPs_CRC),
    .valid_CRC(valid_CRC)
);

endmodule