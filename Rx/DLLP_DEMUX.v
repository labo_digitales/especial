module DLLP_DEMUX(
    input clk, resetn, TLP_DLLP, //0->DLLP, 1->TLP
    input [7:0] DLLP_Demux,
    output reg [7:0] DLLPs, TLPs
);

    always @(posedge clk)begin
        if (!resetn)begin
            DLLPs <= 0;
            TLPs <= 0;
        end
        else begin
            if (TLP_DLLP) begin
                TLPs <= DLLP_Demux;
                DLLPs <= 0;
            end else begin
                DLLPs <= DLLP_Demux;
                TLPs <= 0;
            end
        end
    end

endmodule