`include "ACK_NACK_LATENCY_TIMER.v"
`include "DLLP_DEMUX.v"
`include "NEXT_RECIEVING_SEQ.v"
`include "CRC_CALC_CHECK.v"
`include "LCRC_CALC_CHECK.v"
`include "RX_BUFFER.v"

module Rx(
    input clk, resetn, DLLP_TLP_signal, TLP_start, DLLP_start,
    input [7:0] Tx_in,
    output valid_CRC, Sch_DLLPs,
    output [7:0] DLLPs, TLP_out,
    output [11:0] ACK_NACK_time
);

	reg DLLP_flop, TLP_flop;

	always @(posedge clk)begin
		if (!resetn) begin
			DLLP_flop <= 0;
			TLP_flop <= 0;
		end else begin
			DLLP_flop <= DLLP_start;
			TLP_flop <= TLP_start;
		end
	end

    wire valid_LCRC;
    wire [7:0] TLPs_demux;
    wire [11:0] next_rec_seq;

	DLLP_DEMUX demux(
        /*AUTOINST*/
		     // Outputs
		     .DLLPs		(DLLPs),
		     .TLPs		(TLPs_demux),
		     // Inputs
		     .clk		(clk),
		     .resetn	(resetn),
		     .TLP_DLLP	(DLLP_TLP_signal),
		     .DLLP_Demux(Tx_in));

    LCRC_CALC_CHECK LCRC_check(
        /*AUTOINST*/
			       // Outputs
			       .valid_LCRC	(valid_LCRC),
			       // Inputs
			       .clk			(clk),
			       .resetn		(resetn),
			       .start_TLP	(TLP_flop),
			       .TLPs		(TLPs_demux));

	NEXT_RECIEVING_SEQ next_seq(
        /*AUTOINST*/
				// Outputs
				.NEXT_RECIEVING_SEQ(next_rec_seq),
				// Inputs
				.clk		(clk),
				.resetn		(resetn),
				.valid_data	(valid_LCRC));

    RX_BUFFER rx_buffer(
        /*AUTOINST*/
			// Outputs
			.TLP_TranLayer	(TLP_out),
			.Sch_DLLPs		(Sch_DLLPs),
			// Inputs
			.clk			(clk),
			.resetn			(resetn),
			.valid_data		(valid_LCRC),
			.TLPs			(TLPs_demux),
			.next_rec_seq	(next_rec_seq));

    ACK_NACK_LATENCY_TIMER nack_timer(
        /*AUTOINST*/
				      // Outputs
				      .ACK_NACK_time	(ACK_NACK_time),
				      // Inputs
				      .clk			(clk),
				      .resetn		(resetn),
				      .Sch_DLLPs	(Sch_DLLPs));

	CRC_CALC_CHECK CRC_check(
        /*AUTOINST*/
			     // Outputs
			     .valid_CRC		(valid_CRC),
			     // Inputs
			     .clk			(clk),
			     .resetn		(resetn),
			     .star_DLLP		(DLLP_flop),
			     .DLLPs			(DLLPs));

endmodule
