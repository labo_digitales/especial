module REPLAY_NUM(
    input clk, 
    input reset,
    input retransmissions,

    output reg stop_flag);

    reg [11:0] counter;

    always @(posedge clk) begin
        if(!reset) begin
            counter <= 'h00b;
            stop_flag <= 'b0;
        end
        else begin
            counter <= counter + 1;
            if (counter == 'h11b) begin
                stop_flag <= 'b1;
                counter <= 'h00b;
            end
        end
    end
endmodule