module TLP_MUX(
    input [7:0] TLP,
    input [7:0]  LCRC_Calc,
    input [7:0] Next_Trans_Seq,
    input [1:0] sel,
    input reset,
    input clk,
    output reg [7:0] TLP_out
);

    always @(clk) begin 
        if (!reset) TLP_out <= 0;
        else begin
            if (sel == 00) TLP_out <= Next_Trans_Seq;
            else if (sel == 01) TLP_out <= TLP;
            else if (sel == 10) TLP_out <= LCRC_Calc;
            else if (sel == 11) TLP_out <= 0;
        end
    end

endmodule