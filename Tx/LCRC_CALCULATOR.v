module LCRC_CALCULATOR(
    input clk,
    input reset,
    input [7:0] TLPs,
    output reg [7:0] LCRC_out 
);

    always @(posedge clk) begin
        if (!reset) begin
            LCRC_out = 8'hFF;
        end
        else begin
            LCRC_out[0] <= TLPs[7];
            LCRC_out[6:1] <= TLPs[6:1];
            LCRC_out[7] <= TLPs[0];
        end
    end

endmodule