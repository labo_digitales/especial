`include "CRC_CALCULATOR.v"
`include "DLLP_MUX.v"
`include "LCRC_CALCULATOR.v"
`include "NEXT_TRANSMIT_SEQ.v"
`include "REPLAY_NUM.v"
`include "REPLAY_TIMER.v"
`include "RETRY_BUFFER.v"
`include "TLP_MUX.v"
`include "TRANSMIT_MUX.v"

module TX (
    input clk,          //from TB
    input reset,        //from TB
    input [7:0] TLP,     //from TB
    input tlp_start_end,//from TB
    input [7:0] DLLP,    //from control
    input DLLP_sel,     //from control
	input [1:0] transmit_sel, //from control
	input retransmision,
	input [7:0] TLP_retrans,
    output stop_flag,   //from replay_num
    output retry_control,   //from retry_buffer
    output [7:0] Transmit_out   //from transmit_mux
    );

	wire [7:0] CRC, DLLP_TRANSMIT, TLP_out, NEXT_TRANSMIT_OUT, RETRY_out, LCRC_out;
	wire [11:0] SEQ_NUM;
	wire [1:0] retry_tlp;
	wire retry_replay;

    CRC_CALCULATOR CRC_inst(/*AUTOINST*/
			    // Outputs
			    .CRC		(CRC),
			    // Inputs
			    .DLLP		(DLLP),
			    .clk		(clk),
			    .reset		(reset));
				
    DLLP_MUX DLLP_MUX_inst(/*AUTOINST*/
			   // Outputs
			   .DLLP_out		(DLLP_TRANSMIT),
			   // Inputs
			   .M0			(CRC),
			   .M1			(DLLP),
			   .DLLP_sel    (DLLP_sel),
			   .reset		(reset),
			   .clk			(clk));
    LCRC_CALCULATOR LCRC_CALCULATOR_inst(/*AUTOINST*/
					 // Outputs
					 .LCRC_out		(LCRC_out),
					 // Inputs
					 .clk			(clk),
					 .reset			(reset),
					 .TLPs			(TLP_out));
    NEXT_TRANSMIT_SEQ NEXT_TRANSMIT_SEQ_inst(/*AUTOINST*/
					     // Outputs
					     .NEXT_TRANSMIT_OUT	(NEXT_TRANSMIT_OUT),
					     .SEQ_NUM		(SEQ_NUM),
					     // Inputs
					     .clk		(clk),
					     .reset		(reset),
					     .TLP_flag		(TLP_flag));
    REPLAY_NUM REPLAY_NUM_inst(/*AUTOINST*/
			       // Outputs
			       .stop_flag	(stop_flag),
			       // Inputs
			       .clk		(clk),
			       .reset		(reset),
			       .retransmissions	(retransmissions));
    REPLAY_TIMER REPLAY_TIMER_inst(/*AUTOINST*/
				   // Outputs
				   .timer_out		(timer_out),
				   // Inputs
				   .clk			(clk),
				   .reset		(reset),
				   .tlp_start_end	(tlp_start_end));
    RETRY_BUFFER RETRY_BUFFER_inst(/*AUTOINST*/
				   // Outputs
				   .retry_control	(retry_control),
				   .retry_replay	(retry_replay),
				   .retry_timer		(retry_timer),
				   .retry_LCRC		(retry_LCRC),
				   .retry_TLP 		(retry_tlp),
				   .RETRY_out		(RETRY_out[7:0]),
				   // Inputs
				   .clk			(clk),
				   .reset		(reset),
				   .SEQ_NUM		(SEQ_NUM[11:0]),
				   .tlp_start_end	(tlp_start_end),
				   .TLP_out		(TLP_out[7:0]),
				   .control_retry	(control_retry),
				   .timer_retry		(timer_retry),
				   .timer_out		(timer_out)
				   .retransmision(retransmision),
              	   .TLP_retrans(TLP_retrans));
    TLP_MUX TLP_MUX_inst(/*AUTOINST*/
			 // Outputs
			 .TLP_out		(TLP_out),
			 // Inputs
			 .TLP			(TLP[7:0]),
			 .LCRC_Calc		(LCRC_out),
			 .Next_Trans_Seq	(NEXT_TRANSMIT_OUT),
			 .sel			(retry_tlp),
			 .reset			(reset),
			 .clk			(clk));
    TRANSMIT_MUX TRANSMIT_MUX_inst(/*AUTOINST*/
				   // Outputs
				   .Transmit_out	(Transmit_out),
				   // Inputs
				   .DLLP_MUX		(DLLP_TRANSMIT),
				   .Retry_Buffer	(RETRY_out),
				   .TLP_Mux		(TLP_out),
				   .sel			(transmit_sel),
				   .reset		(reset),
				   .clk			(clk));

endmodule