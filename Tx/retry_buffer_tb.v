`include "RETRY_BUFFER.v"

module retry_buffer_tb();

/*Input*/
reg reset, clk, tlp_start_end, control_retry, timer_retry, timer_out, retransmision;
reg [11:0] SEQ_NUM;
reg [7:0] TLP_out, TLP_retrans;
/*Outputs*/
wire retry_control, retry_replay, retry_timer, retry_LCRC;
wire [1:0] retry_TLP;
wire [7:0] RETRY_out;

initial begin

    $dumpfile("retry_buffer.vcd");
    $dumpvars();
    reset <= 0; 
    tlp_start_end <= 0; 
    control_retry <= 0; 
    timer_retry <= 0;
    timer_out <= 0;
    SEQ_NUM <= 0;
    TLP_out <= 0;
    retransmision <= 0;
    TLP_retrans <= 0;
    @(posedge clk);
    reset <= 1; 
    tlp_start_end <= 0; 
    control_retry <= 0; 
    timer_retry <= 0;
    timer_out <= 0;
    SEQ_NUM <= 0;
    TLP_out <= 0;
    retransmision <= 0;
    TLP_retrans <= 0;
    @(posedge clk);
    reset <= 1; 
    tlp_start_end <= 1; 
    control_retry <= 0; 
    timer_retry <= 0;
    timer_out <= 0;
    SEQ_NUM <= 0;
    TLP_out <= 8'hBA;
    retransmision <= 0;
    TLP_retrans <= 0;
    @(posedge clk);
    reset <= 1; 
    tlp_start_end <= 0; 
    control_retry <= 0; 
    timer_retry <= 0;
    timer_out <= 0;
    SEQ_NUM <= 0;
    TLP_out <= 0;
    retransmision <= 0;
    TLP_retrans <= 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    reset <= 1; 
    tlp_start_end <= 0; 
    control_retry <= 0; 
    timer_retry <= 0;
    timer_out <= 0;
    SEQ_NUM <= 0;
    TLP_out <= 0;
    retransmision <= 1;
    TLP_retrans <= 8'hCC;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    $finish;

end
initial clk <= 0;
always #1 clk <= ~clk;

RETRY_BUFFER retry_buffer(
    /*AUTOINST*/
			  // Outputs
			  .retry_control	(retry_control),
			  .retry_replay		(retry_replay),
			  .retry_timer		(retry_timer),
			  .retry_LCRC		(retry_LCRC),
			  .retry_TLP		(retry_TLP),
			  .RETRY_out		(RETRY_out),
			  // Inputs
			  .clk			(clk),
			  .reset		(reset),
			  .SEQ_NUM		(SEQ_NUM),
			  .tlp_start_end	(tlp_start_end),
			  .TLP_out		(TLP_out),
			  .control_retry	(control_retry),
			  .timer_retry		(timer_retry),
			  .timer_out		(timer_out),
              .retransmision(retransmision),
              .TLP_retrans(TLP_retrans)
              );

endmodule
