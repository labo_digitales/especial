module RETRY_BUFFER(        //Sotres the TLPs transmited to the physical layer.
    input clk, 
    input reset,
    input [11:0] SEQ_NUM,
    input tlp_start_end,
    input [7:0] TLP_out,
    input control_retry,
    input timer_retry,
    input timer_out,
    input retransmision,
    input [7:0] TLP_retrans,

    output reg retry_control,
    output reg retry_replay,
    output reg retry_timer,
    output reg retry_LCRC,
    output [1:0] retry_TLP,
    output [7:0] RETRY_out);

    wire store_adress, transmit_flag;
    wire [2:0] index, readCounter, writeCounter;
    wire EMPTY, FULL;
    wire [7:0] fifo_out;
    reg [2:0] pointers;

    assign RETRY_out = (retransmision == 1'b1) ? TLP_retrans : fifo_out;

receiving receiver( .clk            (clk),
                    .reset          (reset),
                    .tlp_start_end   (tlp_start_end),
                    .retry_TLP       (retry_TLP),
                    .store_adress    (store_adress),
                    .index(index));

/*sending transmiter( .clk        (clk),
                    .reset      (reset),
                    .pointers   (pointers),
                    .index      (index));

pointers_manager manager(   .clk           (clk),
                            .reset         (reset),
                            .index         (index),
                            .TLP           (TLP_out),
                            .transmit_flag (transmit_flag));*/

buffer fifo_buffer( .clk            (clk),
                    .reset          (reset),
                    .data_in        (TLP_out),
                    .Rd             (store_adress),
                    .Wr             (tlp_start_end),
                    .index          (index),
                    .dataOut        (fifo_out),
                    .EMPTY          (EMPTY),
                    .FULL           (FULL),
                    .readCounter    (readCounter),
                    .writeCounter   (writeCounter),
                    .timer_out      (timer_out));
endmodule

module receiving(
    input clk,
    input reset, 
    input tlp_start_end,
    output reg [2:0] index,
    output reg [1:0] retry_TLP,   //to TLP_mux
    output reg store_adress    //to pointers manager (Wr)
);

    always @(posedge clk) begin
        if (!reset) begin
            retry_TLP <= 0;
            store_adress <= 0;
            index <= 0;
        end else begin
            if (tlp_start_end) begin
                index <= index + 1;
            end
            if (tlp_start_end == 0) begin
                retry_TLP <= 2'b11; //Non used vaue in the TLP_MUX
                store_adress <= 1'b0;
            end else if (tlp_start_end == 1) begin
                retry_TLP <= 2'b00;  //next_trans_seq
                store_adress <= 1'b1;
            end 
        end
    end
endmodule

/*module sending(
    input clk,
    input reset,
    input [2:0] pointers, //from pointer_manager
    
    output reg [2:0] index
);
    always @(posedge clk) begin
        if(!reset) index <= 'b00;
        else begin 
            if (pointers != 'b00) index <= pointers;
        end
    end
endmodule

module pointers_manager(
    input clk, 
    input reset,
    input [2:0] index,  //from buffer
    input [7:0] TLP,

    output reg transmit_flag
    );
    reg [7:0] spaces [0:2];

    always @(posedge clk) begin
        if (!reset) begin
            transmit_flag <= 'b0;
        end
        else begin
            transmit_flag <= 'b1;
            spaces[index] <= TLP;
        end
    end
endmodule*/

module buffer(
    input clk, 
    input reset,
    input [7:0] data_in, 
    input Rd,
    input Wr,
    input [2:0] index, 
    input timer_out,

    output reg [7:0] dataOut, 
    output EMPTY,
    output FULL,
    output reg [2:0] readCounter,
    output reg [2:0] writeCounter); 

    reg [7:0] FIFO [0:7];
    reg [2:0] Count = 0; 
    reg read_flop;

    assign EMPTY = (Count==0)? 1'b1:1'b0; 
    assign FULL = (Count==8)? 1'b1:1'b0; 

    always @ (posedge clk) begin
        if (!reset) begin
            readCounter <= 0; 
            writeCounter <= 0; 
            read_flop <= 0;
        end 
        else begin
            read_flop <= Rd;
            if (read_flop ==1'b1 && Count!=0) begin 
                dataOut  <= FIFO[readCounter]; 
                readCounter <= readCounter+1; 
            end 
            else if (Wr==1'b1 && Count<8) begin
                FIFO[writeCounter]  = data_in; 
                writeCounter  <= writeCounter+1; 
            end 
            else if (index != 'b0 && Wr==1'b1 && Count<8) begin
                FIFO[writeCounter]  = data_in; 
                writeCounter  <= writeCounter+1;
            end
            else if (timer_out == 1) begin
                dataOut <= FIFO[7];    //oldest data in the buffer
                end

            else if (writeCounter==8) writeCounter <= 0; 
            else if (readCounter==8) readCounter=0; 
            else;
            if (readCounter > writeCounter) begin 
                Count=readCounter-writeCounter; 
            end 
            else if (writeCounter > readCounter) 
                Count=writeCounter-readCounter; 
            end
        end
endmodule