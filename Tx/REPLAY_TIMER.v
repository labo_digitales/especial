module REPLAY_TIMER(
    input clk,
    input reset, 
    input tlp_start_end,

    output reg timer_out);

    reg [11:0] first_counter;
    reg [11:0] second_counter;
    reg ready_flag;


    always @ (posedge clk) begin
        if (!reset) begin 
            timer_out <= 'b0;
            first_counter <= 'b0;
            second_counter <= 'b0;
            ready_flag <= 'b0;
        end
        else begin
            if (ready_flag == 'b0) begin 
                first_counter <= first_counter + 1;

            if (first_counter == 'd4095) begin
                ready_flag <= 'b1;
                end
            if (ready_flag == 'b1) begin
                second_counter <= second_counter + 1;
            end
            if (second_counter == 'd4095) begin
                timer_out <= 1;
                end
            end 
        end
    end 
endmodule 