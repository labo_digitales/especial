`include "CRC_CALCULATOR.v"
`include "LCRC_CALCULATOR.v"

`include "NEXT_TRANSMIT_SEQ.v"
`include "REPLAY_TIMER.v"
`include "REPLAY_NUM.v"


module tb_test();

reg [7:0] DLLP, TLPs;
reg reset, clk, TLP_flag, retransmissions;
wire [7:0] CRC, LCRC_out, NEXT_TRANSMIT_OUT;
wire stop_flag, timer_out, tlp_start_end;
wire [11:0] SEQ_NUM;

initial begin

    $dumpfile("dumpfile.vcd");
    $dumpvars();
    //reset
    @(posedge clk);
    reset <= 0;
    DLLP <= 0;
    TLPs <= 0;
    @(posedge clk);
    //Start
    reset <= 1;
    DLLP <= 8'b11001100;
    TLPs <= 8'b10010100;
    TLP_flag <= 1;
    @(posedge clk);
    TLP_flag <= 1;
    @(posedge clk);
    TLP_flag <= 1;
    @(posedge clk);
    TLP_flag <= 0;
    @(posedge clk);
    DLLP <= 8'b10101010;//->01010101
    TLPs <= 8'b01110111;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    TLP_flag <= 1;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    $finish;

end
initial clk <= 0;
always #1 clk <= ~clk;

CRC_CALCULATOR crc_calc(
    .DLLP(DLLP),
    .clk(clk),
    .reset(reset),
    .CRC(CRC)
);

LCRC_CALCULATOR lcrc_calc(
    .clk(clk),
    .reset(reset),
    .TLPs(TLPs),
    .LCRC_out(LCRC_out) 
);

REPLAY_NUM REPLAY_NUM_inst(/*AUTOINST*/
			       // Outputs
			       .stop_flag	(stop_flag),
			       // Inputs
			       .clk		(clk),
			       .reset		(reset),
			       .retransmissions	(retransmissions));

REPLAY_TIMER REPLAY_TIMER_inst(/*AUTOINST*/
			   // Outputs
			   .timer_out		(timer_out),
			   // Inputs
			   .clk			(clk),
			   .reset		(reset),
			   .tlp_start_end	(tlp_start_end));

NEXT_TRANSMIT_SEQ NEXT_TRANSMIT_SEQ_inst(/*AUTOINST*/
					     // Outputs
					     .NEXT_TRANSMIT_OUT	(NEXT_TRANSMIT_OUT),
					     .SEQ_NUM		(SEQ_NUM),
					     // Inputs
					     .clk		(clk),
					     .reset		(reset),
					     .TLP_flag		(TLP_flag));

endmodule

