`include "../Tx/Tx.v"
`include "../Rx/Rx.v"

module Data_Flow(
    input clk,
    input reset, 
    //Tx
    input stop_flag,
    input retry_control,
    input [7:0] Transmit_out,
    //Rx
    input valid_CRC,
    input Sch_DLLPs,
    input [7:0] DLLs,
    input TLP_out,
    input [11:0] ACK_NACK_time,
    //Tx
    output reg [7:0] TLP, 
    output reg [7:0] DLLP,
    //Rx
    output reg [7:0] DLLP_TLP_signal,
    output reg [7:0] DLLP_start,
    output reg [7:0] Tx_in
);


Rx rx(
    /*Inputs*/
    .clk(clk), 
    .resetn(reset), 
    .DLLP_TLP_signal(DLLP_TLP_signal), 
    .TLP_start(TLP_start), 
    .DLLP_start(DLLP_start),
    .Tx_in(Tx_in),
    /*Outputs*/
    .valid_CRC(valid_CRC), 
    .Sch_DLLPs(Sch_DLLPs),
    .DLLPs(DLLPs), .
    .TLP_out(TLP_out),
    .ACK_NACK_time(ACK_NACK_time)
);

TX tx(
    /*Inputs*/
    .clk(clk),          //from TB
    .reset(reset),        //from TB
    .TLP(TLP),     //from TB
    .tlp_start_end(tlp_start_end),//from TB
    .DLLP(DLLP),    //from control
    .DLLP_sel(DLLP_sel),     //from control
	.transmit_sel(transmit_sel), //from control
	.retransmision(retransmision),
	.TLP_retrans(TLP_retran),
    /*Outputs*/
    .stop_flag(stop_flag),   //from replay_num
    .retry_control(retry_control),   //from retry_buffer
    .Transmit_out(Transmit_out)
);

endmodule

