import binascii

'''
    Creates a hex file as output from a given input file with no particular format. In this case we create a hex file as output from an known image jpg input file.
'''

file_in = r'./xilinx_logo.jpg'
file_out = r'./xilinx.hex' 
writer = open(file_out, 'w') 
with open(file_in, 'rb') as f:
    content = f.read()

writer.write(f'{binascii.hexlify(content)}')
print('\nhex file successfully created')
f.close()
writer.close()