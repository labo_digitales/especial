module NEXT_TRANSMIT_SEQ(
    input clk,
    input reset, 
    input TLP_flag,
    output reg [7:0] NEXT_TRANSMIT_OUT,
    output reg [11:0] SEQ_NUM
);
    parameter [3:0] first_byte = 'b0000; 

    always @ (posedge clk) begin
        if (!reset) begin
            SEQ_NUM <= 'b0;
            //NEXT_TRANSMIT_OUT <= 'b0;
        end
        else begin
            if (TLP_flag == 1) begin   //DURING THE TRANSMITION
                NEXT_TRANSMIT_OUT [3:0] <= SEQ_NUM[11:8];
                NEXT_TRANSMIT_OUT [7:4] <= first_byte;
                SEQ_NUM <= SEQ_NUM + 1;
            end
            else if(TLP_flag == 0) begin
                SEQ_NUM <= SEQ_NUM + 1;
            end
        end
    end
endmodule
