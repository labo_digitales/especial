module DLLP_MUX(
    input [7:0] M0,
    input [7:0] M1,
    input DLLP_sel,
    input reset,
    input clk,
    output reg [7:0] DLLP_out
    );

    always @(posedge clk) begin 
        if (!reset) DLLP_out <= 8'b0;

        else begin
            if (!DLLP_sel) DLLP_out <= M0;
            else DLLP_out <= M1;
        end
    end

endmodule