module file_readmemh(
     input clk, 
     output reg [7:0] out
);

reg [19:0] data [0:24152];
initial $readmemh("xilinx.hex", data);
integer i;

     always @(posedge clk) begin
     for (i=0; i < 12076; i=i+1) begin
          out[3:0] = data[i];
          out[7:4] = data[i+1];
     end
end
endmodule