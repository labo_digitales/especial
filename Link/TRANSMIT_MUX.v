module TRANSMIT_MUX(
    input [7:0] DLLP_MUX, 
    input [7:0] Retry_Buffer,
    input [7:0] TLP_Mux,
    input [1:0] sel,
    input reset,
    input clk,
    output reg [7:0] Transmit_out
);

    always @(posedge clk) begin
        if (!reset) Transmit_out <= 0;

        else begin
            if (sel == 00) Transmit_out <= DLLP_MUX;
            else if (sel == 01) Transmit_out <= Retry_Buffer;
            else if (sel == 10) Transmit_out <= Retry_Buffer;
            else if (sel == 11) Transmit_out <= 0;
        end
    end

endmodule