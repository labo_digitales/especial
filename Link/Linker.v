`include "ACK_NAK_NOTIFICATIONS.v"
`include "LINK_STATUS_MANAGEMENT.v"
`include "DATA_FLOW_CONTROL.v"
`include "Tx.v"
`include "Rx.v"
`include "tester.v"

module Linker (
    input clk, 
    input reset, 
    input [7:0] TLP_data_in,
    
    output [7:0] TLP_data_out
);

    /*Cables de Rx*/
    reg DLLP_TLP_signal, TLP_start, DLLP_start;
    wire [7:0] Tx_in;
    wire valid_CRC, Sch_DLLPs;
    wire [7:0] DLLPs, TLP_out;
    wire [11:0] ACK_NACK_time;
    /*Cables de Tx*/
    reg [7:0] TLP, DLLP, TLP_retrans, Transmit_out;
    reg tlp_start_end, transmit_sel;
    wire Transmit_out, stop_flag, retry_control;
    /*Cables de Ack_Nak_notif*/
    wire ack_nak_flag;
    /*Cables de Link Status*/
    wire TLayer_en, Link_up, DL_Down;

file_readmemh data_in(
    .clk (clk)
    .out (TLP_data_in));

Rx rx(
    /*Inputs*/
    .clk(clk), 
    .resetn(reset), 
    .DLLP_TLP_signal(DLLP_TLP_signal), 
    .TLP_start(TLP_start), 
    .DLLP_start(DLLP_start),
    .Tx_in(Tx_in),
    /*Outputs*/
    .valid_CRC(valid_CRC), 
    .Sch_DLLPs(Sch_DLLPs),
    .DLLPs(DLLPs), .
    .TLP_out(TLP_out),
    .ACK_NACK_time(ACK_NACK_time)
);

TX tx(
    /*Inputs*/
    .clk(clk),          //from TB
    .reset(reset),        //from TB
    .TLP(TLP),     //from TB
    .tlp_start_end(tlp_start_end),//from TB
    .DLLP(DLLP),    //from control
    .DLLP_sel(DLLP_sel),     //from control
	.transmit_sel(transmit_sel), //from control
	.retransmision(retransmision),
	.TLP_retrans(TLP_retran),
    /*Outputs*/
    .stop_flag(stop_flag),   //from replay_num
    .retry_control(retry_control),   //from retry_buffer
    .Transmit_out(Transmit_out)
);

Ack_Nak_notif Ack_Nak_notif_inst(
    /*Inputs*/
    .clk(clk), 
    .reset(reset),
    .Sch_DLLPs(Sch_DLLPs),
    /*Outputs*/
    .ack_nak_flag(ack_nak_flag)
);

LINK_STATUS_MANAGEMENR LINK_STATUS_MANAGEMENR_inst(
    /*Inputs*/
    .clk(clk), 
    .resetn(resetn), 
    .TLayer_en(TLayer_en), 
    .Link_up(Link_up),
    /*Outputs*/
    .DL_Down(DL_Down), 
    .DL_Up(DL_Up)
);

Data_Flow Data_Flow_inst(/*AUTOINST*/);

endmodule