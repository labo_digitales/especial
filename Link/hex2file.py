import binascii

'''
    Converts from an input hex file to a selected format output file. In this case we use as input the hex file dumped by the simulation of the PCIe Data Link Layer written in hex and we create an output image file named result.jpg
'''

with open(r'./xilinx.hex', 'r') as f, open(r'./result.jpg', 'wb') as fout:
    data = f.read()
    data = data[2:-1]
    print('New file created succesfully')
    fout.write(binascii.unhexlify(data))
