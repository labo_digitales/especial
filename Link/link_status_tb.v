`include "LINK_STATUS_MANAGEMENT.v"
`include "ACK_NAK_NOTIFICATIONS.v"

module link_status_tb();

reg reset, clk, TLayer_en, Link_up, Sch_DLLPs;
wire DL_Down, DL_Up, ack_nak_flag;

initial begin

    $dumpfile("dumpfile.vcd");
    $dumpvars();
    reset <= 0;
    TLayer_en <= 0;
    Link_up <= 0;
    Sch_DLLPs <= 0;
    @(posedge clk);
    reset <= 0;
    TLayer_en <= 0;
    Link_up <= 0;
    Sch_DLLPs <= 0;
    @(posedge clk);
    reset <= 0;
    TLayer_en <= 0;
    Link_up <= 0;
    Sch_DLLPs <= 0;
    @(posedge clk);
    reset <= 1;
    TLayer_en <= 0;
    Link_up <= 0;
    Sch_DLLPs <= 0;
    @(posedge clk);
    reset <= 1;
    TLayer_en <= 0;
    Link_up <= 0;
    Sch_DLLPs <= 1;
    @(posedge clk);
    TLayer_en <= 0;
    Link_up <= 1;
    Sch_DLLPs <= 1;
    @(posedge clk);
    TLayer_en <= 1;
    Link_up <= 0;
    Sch_DLLPs <= 0;
    @(posedge clk);
    TLayer_en <= 1;
    Link_up <= 1;
    Sch_DLLPs <= 1;
    @(posedge clk);
    TLayer_en <= 1;
    Link_up <= 0;
    Sch_DLLPs <= 1;
    @(posedge clk);
    TLayer_en <= 1;
    Link_up <= 1;
    Sch_DLLPs <= 0;
    @(posedge clk);
    Link_up <= 1;
    Sch_DLLPs <= 0;
    @(posedge clk);
    TLayer_en <= 0;
    Link_up <= 0;
    Sch_DLLPs <= 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    $finish;

end
initial clk <= 0;
always #1 clk <= ~clk;

LINK_STATUS_MANAGEMENR linker_FSM(
    .clk(clk),
    .resetn(reset), 
    .TLayer_en(TLayer_en), 
    .Link_up(Link_up),
    .DL_Down(DL_Down), 
    .DL_Up(DL_Up)
);

Ack_Nak_notif nak_not(
    .clk(clk), 
    .reset(reset),
    .Sch_DLLPs(Sch_DLLPs),
    .ack_nak_flag(ack_nak_flag)
);  

endmodule