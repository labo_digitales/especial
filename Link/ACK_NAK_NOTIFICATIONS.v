module Ack_Nak_notif (
    input clk, 
    input reset,
    input Sch_DLLPs,
    output reg ack_nak_flag 
);   //0:ack & 1:nak

always @(posedge clk) begin
    if (!reset) begin
        ack_nak_flag <= 0;            
    end else begin
        if (Sch_DLLPs) begin
            ack_nak_flag <= Sch_DLLPs;    
        end else begin
            ack_nak_flag <= 0;
        end
    end 
end

endmodule