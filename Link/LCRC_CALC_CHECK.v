/*
Esto se esribió asumiendo que primero se manda el TLP y después el LCRC para checkear
*/

module LCRC_CALC_CHECK (
    input clk, resetn, start_TLP, //1->TLP, 0->LCRC
    input [7:0] TLPs,
    output reg valid_LCRC
);

    reg [7:0] LCRC_Checker;
    reg LCRC_flag;

    always @(posedge clk)begin
        if (!resetn) begin
            valid_LCRC <= 0;
            LCRC_Checker <= 0;
            LCRC_flag <= 0;
        end
        else begin
            
            if (start_TLP) begin
                LCRC_Checker[0] <= TLPs[7];
                LCRC_Checker[6:1] <= TLPs[6:1];
                LCRC_Checker[7] <= TLPs[0];
                valid_LCRC <= 0;
                LCRC_flag <= 1;
            end else begin
                if (TLPs == LCRC_Checker && LCRC_flag == 1) begin
                    valid_LCRC <= 1;
                    LCRC_flag <= 0;
                end else begin
                    valid_LCRC <= 0;
                    LCRC_flag <= 0;
                end
            end
        end
    end

endmodule

