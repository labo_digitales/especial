module mem #(
    parameter MEM_SIZE = 4096
)(
    input clk, reset,
    output reg [7:0] data
);

    reg [7:0] memory [0:MEM_SIZE-1];
    integer i;
    initial begin
	    $readmemh("xilinx.hex", memory);
        for (i = 0; i<10; i=i+1) 
            data = memory[i]; 
	end

endmodule