module LINK_STATUS_MANAGEMENR #(
    parameter DL_inactive = 2'b00,
    parameter DL_init = 2'b01,
    parameter DL_active = 2'b10
)(
    input clk, resetn, TLayer_en, Link_up,
    output reg DL_Down, DL_Up
);

    reg [1:0] state;
    reg [1:0] next_state;

    always @(posedge clk) begin
        if (!resetn == 1'b1) begin
            state <= DL_inactive;
        end else begin
            state <= next_state;
        end
    end

    always @(state or TLayer_en or Link_up)begin
        next_state = 2'b00;
        case (state)
            DL_inactive: begin
                if (TLayer_en && Link_up) begin
                    next_state = DL_init;
                end
            end 
            DL_init: begin
                if (Link_up) begin
                    next_state = DL_active;
                end
                else if (!Link_up) begin
                    next_state = DL_inactive;
                end
            end 
            DL_active: begin
                if (!Link_up) begin
                    next_state = DL_inactive;
                end
            end
            default: begin
                next_state = DL_inactive;
            end
        endcase
    end

    always @(state)begin
        case (state)
            DL_inactive: begin
                DL_Down = 1;
                DL_Up = 0;
            end
            DL_init: begin
                DL_Down = 0;
                DL_Up = 1;
            end
            DL_active: begin
                DL_Down = 0;
                DL_Up = 1;
            end
            default: begin
                DL_Down = 1;
                DL_Up = 0;
            end
        endcase
    end

endmodule