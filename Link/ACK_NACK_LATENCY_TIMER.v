module ACK_NACK_LATENCY_TIMER(
    input clk, resetn, Sch_DLLPs,
    output reg [11:0] ACK_NACK_time
);

    always @(posedge clk)begin
        if (!resetn) begin
            ACK_NACK_time <= 0;
        end else begin
            if (Sch_DLLPs) begin
                ACK_NACK_time <= ACK_NACK_time + 1;
            end else begin
                ACK_NACK_time <= 0;
            end
        end
    end

endmodule