module CRC_CALCULATOR(
    input [7:0] DLLP,
    input clk,
    input reset,
    output reg [7:0] CRC
);

always @(posedge clk) begin
    if (!reset) begin
        CRC <= 16'hFFFF;    
    end
    else begin
        CRC[0] <= DLLP[7];
        CRC[1] <= DLLP[6];
        CRC[2] <= DLLP[5];
        CRC[3] <= DLLP[4];
        CRC[4] <= DLLP[3];
        CRC[5] <= DLLP[2];
        CRC[6] <= DLLP[1];
        CRC[7] <= DLLP[0];
    end
end

endmodule