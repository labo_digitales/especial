/*
Esto se esribió asumiendo que primero se manda los 0's y después el DLLP,
después se recibe el CRC en 2 partes, primero los 0's y después el CRC,
para luego ser checkeados
*/

module CRC_CALC_CHECK (
    input clk, resetn, star_DLLP,
    input [7:0] DLLPs,
    output reg valid_CRC
);
    reg CRC_flag;
    reg [15:0] CRC_Checker;
    reg [15:0] in_CRC;

    always @(posedge clk)begin
        if (!resetn) begin
            valid_CRC <= 0;
            CRC_Checker <= 0;
            in_CRC <= 0;
            CRC_flag <= 0;
        end else begin
            if (star_DLLP) begin
                if (in_CRC == CRC_Checker && CRC_flag == 1) begin
                    valid_CRC <= 1;
                end else begin
                    valid_CRC <= 0;
                end
                if (DLLPs == 8'h00) begin
                    CRC_Checker[15:8] <= DLLPs;
                    CRC_flag <= 0;
                end else begin
                    CRC_Checker[0] <= DLLPs[7]; 
                    CRC_Checker[1] <= DLLPs[6];
                    CRC_Checker[2] <= DLLPs[5];
                    CRC_Checker[3] <= DLLPs[4];
                    CRC_Checker[4] <= DLLPs[3];
                    CRC_Checker[5] <= DLLPs[2];
                    CRC_Checker[6] <= DLLPs[1];
                    CRC_Checker[7] <= DLLPs[0];
                    CRC_flag <= 0;
                end
            end else begin
                if (DLLPs == 8'h00) begin
                    in_CRC[15:8] <= DLLPs;
                    CRC_flag <= 0;
                end else begin
                    in_CRC[7:0] <= DLLPs;
                    CRC_flag <= 1;
                end
            end
        end
    end

endmodule